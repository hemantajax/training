(function($) {

  var $doc = $(document),
      $win = $(window),
      $vis,
      docTitle = document.title,
      docEl = document.documentElement,
      bbq = ('bbq' in $),
      address = ('address' in $),
      hasher = bbq && 'bbq' || (address && 'address') || 'noop',
      regSlideNumber = /^-?\d+$/,
      regSlideId = /^#/,
      regHash = /^#\!?\/?/;

  var slideChange = function(event) {
        if (!location.hash) { return true; }
        var slideId = '#' + location.hash.replace(regHash, '');
        if ( $(slideId).find('h1').text() ) {
          document.title = $(slideId).find('h1').text() + ' - ' +  docTitle;
        }
        if ($(slideId).is(':hidden')) {
          $doc.trigger('advance.present', {slide: slideId});
        }
      },
      binds = {
        bbq: function() {
          $win.bind('hashchange', function(event) {
            slideChange(event);
          }).trigger('hashchange');
        },
        address: function() {
          $.address.change(slideChange);
        }
      },
      triggers = {
        bbq: function(id) {
          id = id.replace(/\!?/, '!/');
          location.hash = id;
          $win.trigger('hashchange');
        },
        address: function(id) {
          $.address.value(id);
        }

      };


$.present = function(opts) {

  $.present.init(opts);

};

$.present.options = {
  hashchange: hasher,
  slideClass: 'slide',
  step: true,
  stepClass: 'step',
  stepVisibleClass: 'step-done',
  toc: true,
  slidesUp: 1 // not yet implemented
};

$.present.init = function(opts) {

  opts = $.extend($.present.options, opts);
  opts.slideSel = '.' + opts.slideClass;
  opts.stepSel = '.' + opts.stepClass;
  opts.stepVisSel = '.' + opts.stepVisibleClass;

  $.present.step = opts.step;
  if (!opts.step) {
    $(document).trigger('stepToggle', 'init');
  }

  /* slideshow */
  $(opts.slideSel).each(function(index) {
    if (!this.id) {
      this.id = 'slide-' + index;
    }
  });

  $vis = $(location.hash ?
    '#' + location.href.split(/#\!?\/?/)[1] :
    opts.slideSel + ':first')
  .show().addClass('js-show');

  /* $vis = $('.slide:visible'); */

  /* history management */
  binds[ hasher ]();

  /* table of contents */
  if ($.present.options.toc) {
    $(document).ready(toc);
  }

};

/* slide management */

$doc.bind('advance.present', function(event, adv) {

  var dir = adv.direction || 'next',
      slide = adv.slide || null,
      opts = $.present.options;

  $vis = $(opts.slideSel + ':visible').eq(0);

  var $nextSlide = gotoSlide({slide: slide, visible: $vis, direction: dir}, opts);

  if ($nextSlide.length && !$nextSlide.is(':visible') && !$vis.is(':animated')) {
      tocActive($nextSlide);

      $vis.fadeOut(opts.transitionSpeed);
      $nextSlide
      .fadeIn(opts.transitionSpeed, function() {
        $vis.removeClass('js-show').css('display','');
        $nextSlide.addClass('js-show');
        triggers[ hasher ]( $nextSlide.attr('id') );
      });

      if ($.present.step) {
        $nextSlide.find(opts.stepVisSel).removeClass(opts.stepVisibleClass);
      }

  } else {
    advanceStep({visible: $vis, direction: dir}, opts);
  }

});

function gotoSlide(pres, opts) {

  /* go directly to a slide */
  if ( regSlideNumber.test(pres.slide) ) {
    return $(opts.slideSel).eq( pres.slide );
  } else if ( regSlideId.test(pres.slide) ) {
    return $(pres.slide);
  }

  var $vis = pres.visible;

  /* go to previous or next slide if no steps in visible slide or if stepping is turned off  */
  if ( !$vis.find(opts.stepSel).length || !$.present.options.step ) {
    return $vis[pres.direction](opts.slideSel);
  }
  /* go to next slide */
  if ( ($vis.find(opts.stepSel).length == $vis.find(opts.stepVisSel).length) && pres.direction == 'next' ) {

    return $vis.next(opts.slideSel);
  }

  /* go to previous slide */
  if ( !$vis.find(opts.stepVisSel).length && pres.direction == 'prev') {
    return $vis.prev(opts.slideSel);
  }

  return $(false);
}

function advanceStep(adv, opts) {
  if (adv.direction == 'next') {
    adv.visible.find(opts.stepSel).not(opts.stepVisSel).eq(0).addClass(opts.stepVisibleClass);
  } else if (adv.direction == 'prev') {
    adv.visible.find(opts.stepVisSel + ':last').removeClass(opts.stepVisibleClass);
  }
}

/* set up key events */
var keys = {
  e37: function() {
    $doc.trigger('advance.present', {direction: 'prev'});
  },
  e39: function() {
    $doc.trigger('advance.present', {direction: 'next'});
  },
  e13: function() {
    if ($('body').hasClass('list')) {

      var activeSlide = $(document.activeElement).closest('div.slide'),
          activeIndex = activeSlide.index();
      $('body').removeClass('list');
      location.hash = '#!' + activeSlide.attr('id');
      slideChange();
      $('div.slide').prop('tabIndex', -1).css('display', '');
    }
  },
  e27: function() {
    $('div.slide').each(function(index) {
      $(this).prop('tabIndex', index+1);
    }).filter('.js-show').focus();
    $('body').addClass('list');
  },
  e9: function(event) {
    var dir = event.shiftKey ? 'prev' : 'next';
    $(document.activeElement)
      .closest('div.slide').addClass('js-show')
        .siblings('.js-show').removeClass('js-show');
  }
};

$doc.bind('keyup.present', function(event) {
  if ($(event.target).is('input, button, textarea, select')) { return false; }
  var w = 'e' + event.which;
  (w in keys) && keys[w](event);
});


/* TABLE OF CONTENTS */

function toc() {
  var enterDelay, leaveDelay,
      $toc = $('<ul class="group"></ul>'),
      tocItems = [];

  $('.slide').each(function(index) {

    tocItems.push('<li><a href="#' + this.id + '" title="' + $(this).find('h1').text() + '">' + (index + 1) + '</a></li>');
  });

  $toc.html(tocItems.join(''));

  if (!$('.toc').length) {
    $('<div class="toc"></div>')
    .append($toc)
    .appendTo('body');
  }
  $('<a></a>', {
    href: '#toc',
    id: 'toc-toggle',
    'class': 'toc-toggle',
    text: '+'
  }).appendTo('body');


  $toc.parent().height($toc.parent().height());
  $toc.css({position: 'absolute', display: 'none'});

  // // toggle on hover
  // $toc.parent().bind('mouseenter.present mouseleave.present', function(event) {
  //   if (event.type == 'mouseenter') {
  //     clearTimeout(leaveDelay);
  //     enterDelay = setTimeout(function() {
  //       $toc.stop(true, true).slideDown();
  //       $('.indicator').removeClass('ui-icon-circle-plus').addClass('ui-icon-circle-minus');
  //     }, 120);
  //   } else if (event.type == 'mouseleave') {
  //     clearTimeout(enterDelay);
  //     leaveDelay = window.setTimeout(function() {
  //       $toc.stop(true, true).slideUp();
  //       $('.indicator').removeClass('ui-icon-circle-minus').addClass('ui-icon-circle-plus');
  //     }, 1000);
  //   }
  // });

  // toggle on click
  $('#toc-toggle').bind('click', function(event) {
    event.preventDefault();

    var $toggle = $(this),
        ttext = $toggle.text();

    $toc.stop(true, true).slideToggle(300, function() {
      $toggle.text(ttext == '+' ? ' -' : '+');
    });
  });
  $toc.find('a').live('click', function() {
    if (this.hash) {
      $doc.trigger('advance', {slide: this.hash});
    } else if (this.value) {
      location.href = this.value;
    }
    $(this).blur();
    return false;
  });

  tocActive($vis);
}

function tocActive(selector) {
  $('.toc a.ui-state-active').removeClass('ui-state-active');
  $('.toc a').eq(+$('.slide').index(selector)).addClass('ui-state-active');
}


$.present.up = function(num) {
  num = num || 4;
  var $slides = $('div.slide'),
      slideSet, slideStamp;

  $('body').addClass('up');
  $slides.addClass('up-' + num);

  $slides.each(function(index) {
    if ( index % num === 0 ) {
      if (slideSet) {
        slideSet.appendTo('body');
      }
      slideStamp = +new Date;
      slideSet = $('<div></div>').prop('id', 'slide-' + slideStamp).addClass('slide');
    }
    if ($(this).is('.js-show')) {
      slideSet.addClass('js-show');
    }
    slideSet.append(this);
  });
};
$(document).bind('stepToggle', function(event, init) {
  if (!init) {
    $.present.options.step = !$.present.options.step;
  }
  $('body').toggleClass('step-off', !$.present.options.step);

});

})(jQuery);