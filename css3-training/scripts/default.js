// run the presentation script
$(document).ready(function() {
  $.present();
});

$('a').live('click.present', function() {
  var thisHref = $(this).attr('href');

  if ( (thisHref && this.hostname != location.hostname) || !this.hash) {
    window.open(thisHref);
    return false;
  }
});


$(document).ready(function(event) {

  /* code highlighting */
   $('pre').chili();

  /* silly blink tag gimmick */
  $('blink').live('click', function() {
    $(this).replaceWith('<em>' + $(this).text() + '</em>');
  });
  $('.mailto').html('<a href="mailto:karl@learningjquery.com">karl@learningjquery.com</a>');
  /* THEMEROLLER Ready */
  var placeholders;
  $.trReady = function() {
    if (!placeholders) {
      $('.feature-wrapper').addClass('fwp');
      $('.feature').addClass('fp');
      placeholders = true;
    }
    var $slide = $('.slide');
    $slide.toggleClass('ui-widget').toggleClass('ui-widget-content');
    $slide.find('h1').toggleClass('ui-widget-header');

    $slide.find('.fwp')
    .toggleClass('feature-wrapper');

    $slide.find('.fwp, ul')
    .toggleClass('ui-widget-content')
    .toggleClass('ui-corner-all');

    $slide.find('.fp').toggleClass('feature');
    $('.toc a').toggleClass('ui-state-default').toggleClass('ui-corner-all');
  };
});

