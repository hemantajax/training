/** =remove images
************************************************************/
var easings = [
  {
    right: 'easeOutBounce',
    bottom: 'easeOutElastic'
  },
  {
    right: 'easeInElastic',
    bottom: 'easeInBounce'
  }
];
$('img').bind('dblclick', function(event) {
  var $img = $(this),
      isButterfly = this.src.indexOf('butterfly') != -1,
      off = $img.offset(),
      h = $img.height(),
      w = $img.outerWidth(),
      l = off.left,
      t =  off.top,
      props = [
        {
          right: l / 2,
          bottom: t / 2
        },
        {
          right: l + (isButterfly ? w : 0),
          bottom: t + (isButterfly ? h : 0)
        }
      ];

  $img.css({
    position: 'absolute',
    right: 0,
    bottom: 0
  }).animate(props[0], {duration: 1000, specialEasing: easings[0]})
  .animate(props[1], {duration: 1000, specialEasing: easings[1]});
  if ( !isButterfly ) {
    $img.effect('explode', {pieces: 27, easing: 'easeOutQuint'});
  }

});

/** =delegate
************************************************************/
var colormeblue = function() {
      $(this).css('color', 'blue');
    },
    mybgyellow = function() {
      $(this).css('backgroundColor', 'yellow');
    },
    injectddiv = function() {
      $('#dwrapper').html('#dwrapper');
      $('<div></div>', {
        id: 'ddiv',
        html: '#ddiv <span>span</span>'
      }).appendTo('#dwrapper');

    };

$('#dwrapper').delegate('span', 'click', colormeblue);
$('#ddiv').delegate('span', 'click', mybgyellow);

$('#dbtn').bind('click', injectddiv);


  /** =callbacks
  ************************************************************/


  function myCallback(el, id) {
    $(el).closest('div.slide').find('div.log').append('<div>called after ' + id + '</div>');
  }

  $.each(['slideDown', 'slideUp', 'slideToggle'], function(i, id) {
    $('#' + id).bind('click', function() {
      $('div.cb')[ id ](400, function() {
        myCallback(this, id);
      });
    });

    $('#p-' + id).bind('click', function() {
      $('div.pcb')[ id ](400).promise().done(function() {
        myCallback(this, id);
      });
    });
  });

  /** =Repeating callbacks
  ************************************************************/
  $('#repeat').click(function() {
    var $sequence = $('div.sequence'),
        index = 0;

    (function sequencer() {
        $sequence.eq(index++)
        .animate({opacity: 'toggle'}, 'slow', sequencer);
    })();
  });

  $('#pulser').click(function() {
    var $pulse = $('div.pulse'),
        index = 0,
        total = 10;

    (function pulse() {
      if (index++ < total) {
        $pulse
        .animate({opacity: 'toggle'}, pulse);
      }
    })();
  });

  var colors = ['#900', '#090', '#009', '#cc0', '#0cc', '#c0c'];
  var pulseOpts = {
    total: 9,
    duration: 489,
    complete: function(i) {
      this.style.backgroundColor = colors[i % 6];
    }
  };
  $('#pulseer').click(function() {
    $('div.pulsee').slice(0,5).pulsee(11);
    $('div.pulsee').slice(5).pulsee(pulseOpts);
  });


  /** =Pulsee Plugin (repeating callbacks)
  ************************************************************/

  $.fn.pulsee = function(options) {
    var counter = 0, self = this;
    var opts = $.extend({},
      $.fn.pulsee.defaults,
      typeof options === 'number' ? {total: options} : options || {}
    );

    // make sure we're working with a number
    opts.total = parseInt(opts.total, 10) || 2;

    (function pulse() {
      if (counter++ < opts.total) {
        self.animate({opacity: 'toggle'}, opts.duration, opts.easing, function() {
          opts.complete.call(this, counter-1);
          pulse();
        });
      }
    })();

    return self;
  };
  $.fn.pulsee.defaults = {total: 10, complete: $.noop};


/** =selectors.php
************************************************************/
$(document).ready(function() {
  $("#selectors form").submit(function() {
    var selector = $("#selector").val();

    $("#selectors pre, #selectors div.sample")
      .find("*").removeClass("found").end()
      .find(selector).addClass("found");
    return false;
  });
});

$('pre.javascript').live('dblclick', function(event) {
  $(this).addClass('editable').attr('contentEditable', true);
  if ($.fn.resizable) {
    $(this).wrap('<div></div>').parent().addClass('prewrap');
    $(this).resizable();
  }
});
/** =do-something.php
************************************************************/

$(document).ready(function() {

  $('#addclass-special button').click(function() {
    $('#addclass-special li:nth-child(odd)').addClass('special');
  });


  $('#after button').click(function() {
    $('#after a[href$=.pdf]')
      .after(' <img src="images/pdf.png" alt="PDF" />');
  });

  $('#css button').click(function() {
    $('#css a').css({
      color: 'red',
      fontWeight: 'bold',
      'background-color': '#ff3'
    });
  });



  $('#submit-1 form').submit(function() {
    if ( $('#name').val() == '' ) {
      $('span.help').show();
      return false;
    }
  });


  $('#click a.menu').click(function() {
    $(this).next().toggle();
    return false;
  });

  $('#slideToggle a.menu').click(function() {
    $(this).next().slideToggle('slow');
    return false;
  });

  $('#animate button').click(function() {
    $('#animate div.block').animate({
      fontSize: '2em',
      width: '+=20%',
      backgroundColor: 'green'
    });
  });

  $('#hideshow button').click(function() {
    $('#hideshow div.block').hide('slow', function() {
      $(this).show('slow');
    });
  });

  $('#load button').click(function() {
    $('div.load').load('file.html');
  });

  $('#load2 button').click(function() {
    $('#load2 div.load').load('file.html h2');
  });

  $('#live-table tr')
  .bind('click', function() {
    $(this).css({color: '#f00'});
  })
  .live('click', function() {
    $(this).css({backgroundColor: '#ff0'});
  });

  $('#add-row').click(function() {
    $('#live-table tr:last').after('<tr><td colspan="3">new row!</td></tr>');
    return false;
  });

  $('#html button').click(function() {
    $('<li><a></a></li>')
      .find('a')
      .attr('href', 'http://www.learningjquery.com/')
      .html('Learn More')
    .end()
    .appendTo('#html ul');

  });

  $("#row-striping form").submit(function() {
    var $thisSlide = $(this).parents('.slide');
    var selector = $("#stripe").val();

    $thisSlide.find('.alt').removeClass('alt');
    $thisSlide.find(selector).addClass('alt');
    return false;
  });
});

/** =plugins.php
************************************************************/
$(document).ready(function() {
  $('#themeroller form').submit(function() {
    $.trReady();
    return false;
  });
});

$('#tail').live('dblclick', function(event) {
  $(this).effect('explode', function() {
    $(this).remove();
  });
});
